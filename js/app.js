/*
 * declare Gloabel Varibale
*/
var pageContain = document.getElementById("container");
var matchCards  = document.getElementsByClassName("match");
var starsList   = document.querySelectorAll(".stars li");
var counter     = document.querySelector(".moves");
var card        = document.getElementsByClassName("card");
var rating      = document.getElementById("finalRating");
var stars       = document.querySelectorAll(".fa-star");
var starsLi     = document.querySelector(".stars");
var timer       = document.querySelector(".timer");
var deck        = document.getElementById("deck");
var winner      = document.getElementById("winner");
var cards       = [...card];
var openCards   = [];
var moves       = 0;
var second      = 0;
var minute      = 0; 
var hour        = 0;
var interval;

/*
 *Shuffle Ronadoam Cards
*/
shuffle=(array)=> {
    var currentIndex = array.length, temporaryValue, randomIndex;

    while (currentIndex !== 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

/*
 *shuffled cards when page is refreshed
*/
document.body.onload = startGame();


/*
 *start a new play and reset Value
*/
function startGame(){
    cards = shuffle(cards);

    /*remove all exisiting classes cards*/
    deck.innerHTML = "";
    cards.forEach(function(card) {
        deck.appendChild(card);
        card.classList.remove("show", "open", "match", "disabled", "flipWrong");
    });

    /*reset moves Conter*/
    moves = 0;
    counter.innerHTML = moves;

    /*reset rating stars*/
    for (var i= 0; i < stars.length; i++) {
        stars[i].style.color = "#ffd700";
        stars[i].style.visibility = "visible";
    }

    /*reset timer*/
    second = 0;
    minute = 0; 
    hour = 0;
    timer.innerHTML = "0 Mins 0 Secs";
    clearInterval(interval);
}


/*
 *flipping Cards and check is match or not
*/
cards.forEach(function(card) {
    card.addEventListener('click', function(e) {
        if(card.classList.contains('open') && card.classList.contains('show') || card.classList.contains('match')){
            disable();
            enable();
        }else{
            openCards.push(this); 
            card.classList.add('open', 'show');

            if(openCards.length === 2){   //Check How many card opend
                moveCounter();
                if(openCards[0].type === openCards[1].type) {
                   match();
                }else {
                   unmatch();
                }     
            }
            if(matchCards.length == 16) {
                sucessMessage();
            }
        }    
    });
});

/*
 *increment Move Counter
*/
function moveCounter(){
    moves++;
    counter.innerHTML = moves;
    
    //Timer Begin When I 1 Move
    if(moves == 1){
        second = 0;
        minute = 0;
        hours  = 0;
        startTimer();

    }else if(moves > 8 && moves < 15) {
        stars[2].style.visibility = "hidden";

    }else if(moves > 15) {
        stars[1].style.visibility = "hidden";
    }
}

/*
 *Start Timer Function
*/
function startTimer() {
    interval = setInterval(function() {
        timer.innerHTML = minute+" Mins  "+second+" Secs";
        second++;
        if(second == 60) {
            minute++;
            second = 0;

        }if(minute == 60) {
            hours++;
            minute = 0;

        }if(minute == 1) { 
            stars[2].style.visibility = "hidden";
            
        }if(minute == 3) {
            stars[1].style.visibility = "hidden";
        }
        
    },1050);
}

/*
 *check card which is match
*/
function match() { 
    cards.forEach(function(card) {
        card.classList.remove("flipWrong");
    }); 
    openCards[0].classList.add("match");
    openCards[1].classList.add("match");
    disable();
    setTimeout(function() {
        openCards[0].classList.remove("open", "show");
        openCards[1].classList.remove("open", "show");
        enable();
        openCards = [];
    },500);
}

/*
 *check card which is not match
*/
function unmatch() {
    cards.forEach(function(card) {
        card.classList.remove("flipWrong");
    }); 
    openCards[0].classList.add("unmatch");
    openCards[1].classList.add("unmatch");
    disable();
    setTimeout(function() {
        openCards[0].classList.remove("open", "show", "unmatch");
        openCards[1].classList.remove("open", "show", "unmatch");
        openCards[0].classList.add("flipWrong");
        openCards[1].classList.add("flipWrong");
        enable();
        openCards = [];
    },1000);
     
}

/*
 *disable cards temporarily
*/
function disable() {
    cards.forEach(function(card) {
        card.classList.add('disabled');
    });
}

/*
 *enable cards and disable matched cards
*/
function enable() {
    cards.forEach(function(card) {
        card.classList.remove('disabled');
        for(var i = 0; i < matchCards.length; i++) {
            matchCards[i].classList.add("disabled");
        }
    });
}

/*
 *Sucess Mesage Function
*/
function sucessMessage(){
    clearInterval(interval);
    var finalTimes = timer.innerHTML;
    var finalMoves = counter.innerHTML;
    var finalStars = document.querySelector(".stars").innerHTML;
    pageContain.classList.add("hidden");
    winner.classList.add("show");
    document.getElementById("finalMoves").innerHTML  = finalMoves + " Moves";
    document.getElementById("finalTimes").innerHTML  = finalTimes ;
    document.getElementById("finalRating").innerHTML = finalStars ;
}
/*
 *Play Again Function
 */
function playAgain() {
    pageContain.classList.remove("hidden");
    rating.classList.add("hidden");
    winner.classList.remove("show");
    startGame();
}